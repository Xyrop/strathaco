var Thaco = Thaco || {}

Thaco.nbRows     = 0
Thaco.nbCols     = 0
Thaco.nbCells    = 0
Thaco.nbGroups   = 0
Thaco.nbSections = 0

/////////
// Row //
/////////

Thaco.Row = class {

	constructor(text) {
		this.id        = Thaco.nbRows++
		this.text      = text || ''
		this.themes    = new Thaco.Section(Thaco.Theme)
		this.locations = new Thaco.Section(Thaco.Location)
		this.npcs      = new Thaco.Section(Thaco.NPC)
		this.items     = new Thaco.Section(Thaco.Item)
	}

	get sections() {
		let sections = Array()
		sections.push(this.themes)
		sections.push(this.locations)
		sections.push(this.npcs)
		sections.push(this.items)
		return sections
	}

	push(col) {
		if (col instanceof Thaco.Theme) 
			this.themes.push(col)
		else if (col instanceof Thaco.Location) 
			this.locations.push(col)
		else if (col instanceof Thaco.NPC) 
			this.npcs.push(col)
		else if (col instanceof Thaco.Item)
			this.items.push(col)

		return col
	}

	static get name() {
		return 'Row'
	}	
}

Thaco.Character = class extends Thaco.Row {

	static get name() {
		return 'Character'
	}	

	static get tableName() {
		return 'Player Characters'
	}	
}
Thaco.Quest = class extends Thaco.Row {

	static get name() {
		return 'Quest'
	}	

	static get tableName() {
		return 'Scenario'
	}	
}

/////////////
// Section //
/////////////

Thaco.Section = class extends Array {
	constructor(objType) {
		super()
		this.id = Thaco.nbSections++
		this.objType = objType
	}

	addempty() {
		super.push(new this.objType())
	}

	push(e) {
		if (e instanceof this.objType)
			super.push(e)
		else 
			super.push(new this.objType(...arguments))
	}
}

/////////
// Col //
/////////

Thaco.Col = class {

	constructor(args) {
		this.id          = Thaco.nbCols++
		this.background_ = new Thaco.Background()
		this.objective_  = new Thaco.Objective()

		if (arguments.length == 1) {
			if (args.background)
				this.background = args.background
			if (args.objective)
				this.objective  = args.objective
		}
		else if (arguments.length == 2) {
			this.background = arguments[0]
			this.objective  = arguments[1]
		}

	}

	set background(arg) {
		if (arg instanceof Thaco.Cell)
			this.background_ = arg
		else
			this.background_ = new Thaco.Background(arg)
	}

	set objective(arg) {
		if (arg instanceof Thaco.Cell)
			this.objective_ = arg
		else
			this.objective_ = new Thaco.Objective(arg)
	}

	get background() {return this.background_}
	get objective() {return this.objective_}
}

Thaco.Theme    = class extends Thaco.Col {
	static get name() {
		return `Theme`
	}
}

Thaco.Location = class extends Thaco.Col {
	static get name() {
		return `Location`
	}
}

Thaco.NPC      = class extends Thaco.Col {
	static get name() {
		return `NPC`
	}
}

Thaco.Item     = class extends Thaco.Col {
	static get name() {
		return `Item`
	}
}

//////////
// Cell //
//////////

Thaco.Cell = class {
	constructor(text) {
		this.id = Thaco.nbCells++
		this.group = null
		this.text = text
	}
}

Thaco.Objective  = class extends Thaco.Cell {}
Thaco.Background = class extends Thaco.Cell {}

///////////
// Group //
///////////

Thaco.Group = class {

	constructor() {
		this.color = Thaco.getRandomColor()
		this.id = Thaco.nbGroups++
		this.cells = Array()
	}
}

////////////
// Utils  //
////////////

Thaco.getRandomColor = function() {
  var letters = '89ABCD';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 6)];
  }
  return color;
}

Thaco.addCellToGroup = function(cell, group) {
	if (cell.group)
		Thaco.removeCellFromGroup(cell, cell.group)
	cell.group = group
	if (group) 
		group.cells.push(cell)
}

Thaco.removeCellFromGroup = function(cell, group) {
	cell.group = null
	var i = group.cells.indexOf(cell);
	if (i > -1) {
	  group.cells.splice(i, 1);
	}
	return i
}

Thaco.rowToJSON = function(row) {
	let json = {}
	json.name = row.name
	json.text = row.text
	json.themes = row.themes.map(col => Thaco.colToJSON(col))
	json.locations = row.locations.map(col => Thaco.colToJSON(col))
	json.npcs = row.npcs.map(col => Thaco.colToJSON(col))
	json.items = row.items.map(col => Thaco.colToJSON(col))
}

Thaco.colToJSON = function(col) {
	let json = {}
	json.name = col.name
	json.background = col.background
	json.objective = col.objective
	return json
}